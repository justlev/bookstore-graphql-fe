import React, { Component } from 'react';
import {graphql} from 'react-apollo';
import {getAuthorsQuery} from '../queries/queries';

//TODO: Move to proper state management once lecture is complete

class AddBook extends Component {

    constructor(props){
        super(props);
        this.state = {
            name: '',
            genre: '',
            authorId: ''
        }
    }

    displayAuthors(){
        const data = this.props.data;
        if (data.loading){
            return <option>Loading Authors...</option>
        }
        return data.authors.map((author) => {
            return <option key={author.id}>{author.name}</option>
        })
    }

    submitForm(e){
        e.preventDefault();
        console.log(this.state)
    }

    render() {
        console.log(this.props);
      return (
        <form id="add-book" onSubmit={this.submitForm.bind(this)}>
        <div className="field">
            <label>Book Name:</label>
            <input type="text" onChange={(e) => this.setState({name: e.target.value})} />
        </div>
    
        <div className="field">
            <label>Genre: </label>
            <input type="text" onChange={(e) => this.setState({genre: e.target.value})} />
        </div>
    
        <div className="field">
                <label>Author: </label>
                <select onChange={(e) => this.setState({authorId: e.target.value})}>
                    {this.displayAuthors()}
                </select>
        </div>
    
        <button>+</button>
    </form>
      );
    }
  }
  
  export default graphql(getAuthorsQuery)(AddBook);
  